export interface MyComponentProps {
  imageString: string
  nameCategorie: string
  linkAction: string
  intl: any
}
