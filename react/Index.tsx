/* eslint-disable no-console */
import React from 'react'
import { injectIntl } from 'react-intl'
import { useCssHandles } from 'vtex.css-handles'
import { MyComponentProps } from './typings/global'

//Declare Handles for the react component to be accesible
const CSS_HANDLES = [
  'someHandle1',
  'someHandle2',
] as const

const MyComponent: StorefrontFunctionComponent<MyComponentProps> = ({
  nameCategorie,
  imageString,
  linkAction
}) => {
  const handles = useCssHandles(CSS_HANDLES)

  return (
    <div>
      {/* Editable props on SiteEditor */}
      <div>
        <a href={linkAction}><img src={imageString} /></a>
        <a href={linkAction} className={`${handles.someHandle2}`}><h4 className={`${handles.someHandle1}`}>{nameCategorie}</h4></a>
      </div>
    </div>
  )
}

//This is the schema form that will render the editable props on SiteEditor
MyComponent.schema = {
  title: 'Categorias Home',
  description: 'Categorias Home',
  type: 'object',
  properties: {
    nameCategorie: {
      title: 'Texto categoria',
      type: 'string',
      default: 'SomeString default value',
    },
    imageString: {
      title: 'URL Imagen',
      type: 'string'
    },
    linkAction: {
      title: 'URL LINK',
      type: 'string'
    }
  },
}

export default injectIntl(MyComponent)
